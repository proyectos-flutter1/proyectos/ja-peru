# ja_peru

Aplicativo móvil con el contenido digital de Junior Achievement Perú

🚀 Link para Descargar el proyecto (App no Oficial): (https://play.google.com/store/apps/details?id=com.JuniorAchievement.ja_peru)

(https://youtu.be/gAdBpv6uF9g)

[![Japeru](https://i.imgur.com/aAJmovj.png)](https://youtu.be/gAdBpv6uF9g)
[![Japeru](https://i.imgur.com/GK0RNTI.png)](https://youtu.be/gAdBpv6uF9g)
[![Japeru](https://i.imgur.com/tvIueUS.png)](https://youtu.be/gAdBpv6uF9g)

:+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)
