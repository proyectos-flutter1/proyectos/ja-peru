import 'package:flutter/material.dart';
import 'package:ja_peru/src/models/podcast_model.dart';
import 'package:ja_peru/src/providers/podcast_provider.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

import 'package:ja_peru/src/theme/theme.dart';
import 'package:provider/provider.dart';

class PodcastIdPage extends StatefulWidget {
  @override
  PodcastIdPageState createState() => PodcastIdPageState();
}

class PodcastIdPageState extends State<PodcastIdPage> {
  int cambio;
  int cambio2;
  int cambio3;
  var color1;
  var color2;

  final podcastProvider = new PodcastProvider();

  PodCastModel podcast = new PodCastModel();

  final estiloTitulo = TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold);
  final estiloSubTitulo = TextStyle(fontSize: 18.0, color: Colors.grey);

  bool playing = false; // at the begining we are not playing any song
  IconData playBtn = Icons.play_arrow; // the main state of the play button icon

  //Now let's start by creating our music player
  //first let's declare some object
  AudioPlayer _player;
  AudioCache cache;

  Duration position = new Duration();
  Duration musicLength = new Duration();

  //we will create a custom slider

  Widget slider() {
    final _screenSize = MediaQuery.of(context).size;
    return Container(
      width: _screenSize.width * 0.7,
      child: Slider.adaptive(
          activeColor: Colors.blue[800],
          inactiveColor: Colors.grey[350],
          value: position.inSeconds.toDouble(),
          max: musicLength.inSeconds.toDouble(),
          onChanged: (value) {
            seekToSec(value.toInt());
          }),
    );
  }

  Widget _hotelTarjeta() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: FadeInImage(
                image: NetworkImage(
                    'https://media-cdn.tripadvisor.com/media/photo-s/13/1d/f0/60/hotel-cocos-inn.jpg'),
                placeholder: AssetImage('assets/no-image.jpg'),
                height: 150.0,
                width: 110,
                fit: BoxFit.cover,
              ),
            ),
            Text(
              'Hoteles',
              overflow: TextOverflow.ellipsis,
            )
          ],
        ));
  }

  //let's create the seek function that will allow us to go to a certain position of the music
  void seekToSec(int sec) {
    Duration newPos = Duration(seconds: sec);
    _player.seek(newPos);
  }

  //Now let's initialize our player
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _player = AudioPlayer();
    cache = AudioCache(fixedPlayer: _player);

    //now let's handle the audioplayer time

    //this function will allow you to get the music duration
    _player.onDurationChanged.listen((Duration d) {
      print('Max duration: $d');
      setState(() => musicLength = d);
    });

    //this function will allow us to move the cursor of the slider while we are playing the song
    _player.onAudioPositionChanged.listen((Duration p) => {
          print('Current position: $p'),
          setState(() => position = p),
        });
  }

  @override
  Widget build(BuildContext context) {
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;
    if (appTheme2.scaffoldBackgroundColor == Color(0xff16202B)) {
      color1 = Colors.black;
      color2 = Colors.blue;
    } else {
      color1 = Colors.blue[800];
      color2 = Colors.blue[200];
    }

    final _screenSize = MediaQuery.of(context).size;

    final dynamic data = ModalRoute.of(context).settings.arguments;
    int pos = data[1];
    final List<PodCastModel> podnew = data[0];

    int cantidad = podnew.length;

    //final PodCastModel podcData = ModalRoute.of(context).settings.arguments;

    if (podnew[0] != null) {
      podcast = podnew[0];
    }
    if (podcast.posicion == 0) {
      final PodCastModel podcData = podnew[pos];
      podcast.fotoPod = podcData.fotoPod;
      podcast.descripcin = podcData.descripcin;
      podcast.titulo = podcData.titulo;
      podcast.ponente = podcData.ponente;
      podcast.linkAudio = podcData.linkAudio;
      podcast.id = podcData.id;
      podcast.posicion = podcData.posicion;
    }
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('J-Lab Podcast'),
          centerTitle: true,
          leading: IconButton(
            iconSize: 25,
            icon: const Icon(Icons.arrow_back_rounded),
            onPressed: () {
              Navigator.of(context).pop();
              _player.stop();
              podcast.fotoPod = "";
              podcast.descripcin = "";
              podcast.titulo = "";
              podcast.ponente = "";
              podcast.linkAudio = "";
              podcast.id = "";
              podcast.posicion = 0;
            },
            //onPressed: () {Navigator.pushNamed(context, 'jlabPodcast');_player.stop(); },
          ),
        ),

        //drawer: MenuWidget(),
        body: Container(
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  color1,
                  color2,
                ]),
          ),
          child: ListView(
            children: <Widget>[
              _datosSuperior(podcast),
              SizedBox(
                height: 30,
              ),
              _imagenPortada(podcast),
              SizedBox(
                height: 40.0,
              ),
              _crearTexto(podcast),
              SizedBox(
                height: 40.0,
              ),
              _imagenSlider(podcast, podnew, cantidad),
              Container(height: 45, color: Colors.white),
            ],
          ),
        ),
      ),
    );
  }

  Widget _datosSuperior(PodCastModel podcast) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10),
            //Let's add some text title
            Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Text(
                '${podcast.titulo}',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.only(left: 12.0),
              child: Text(
                '${podcast.ponente}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24.0,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        ));
  }

  Widget _imagenPortada(PodCastModel podcast) {
    return Center(
      child: Container(
        width: 280.0,
        height: 280.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            image: DecorationImage(
              image: NetworkImage(podcast.fotoPod),
            )),
      ),
    );
  }

  Widget _imagenSlider(
      PodCastModel podcast, List<PodCastModel> podnew, int cantidad) {
    cambio = podcast.posicion;

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          //Let's start by adding the controller
          //let's add the time indicator text

          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "${position.inMinutes}:${position.inSeconds.remainder(60)}",
                  style: TextStyle(fontSize: 18.0, color: Colors.black54),
                ),
                slider(),
                Text(
                  "${musicLength.inMinutes}:${musicLength.inSeconds.remainder(60)}",
                  style: TextStyle(fontSize: 18.0, color: Colors.black54),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                iconSize: 45.0,
                color: Colors.blue,
                onPressed: () {
                  setState(() {
                    if (cambio == cantidad - 1) {
                      cambio = cambio;
                    } else {
                      cambio3 = cambio + 1;
                      final PodCastModel podcData2 = podnew[cambio3];
                      podcast.fotoPod = podcData2.fotoPod;
                      podcast.descripcin = podcData2.descripcin;
                      podcast.titulo = podcData2.titulo;
                      podcast.ponente = podcData2.ponente;
                      podcast.linkAudio = podcData2.linkAudio;
                      podcast.id = podcData2.id;
                      podcast.posicion = cambio3;

                      _player.stop();
                      playBtn = Icons.play_arrow;
                      playing = false;
                      seekToSec(0);
                    }
                  });
                },
                icon: Icon(
                  Icons.skip_previous,
                ),
              ),
              IconButton(
                iconSize: 62.0,
                color: Colors.blue[800],
                onPressed: () {
                  //here we will add the functionality of the play button
                  if (!playing) {
                    //now let's play the song
                    _player.play(podcast.linkAudio);
                    setState(() {
                      playBtn = Icons.pause;
                      playing = true;
                    });
                  } else {
                    _player.pause();
                    setState(() {
                      playBtn = Icons.play_arrow;
                      playing = false;
                    });
                  }
                },
                icon: Icon(
                  playBtn,
                ),
              ),
              IconButton(
                iconSize: 45.0,
                color: Colors.blue,
                onPressed: () {
                  setState(() {
                    if (cambio == 1) {
                      cambio = cambio;
                    } else {
                      cambio2 = cambio - 1;
                      final PodCastModel podcData3 = podnew[cambio2];
                      podcast.fotoPod = podcData3.fotoPod;
                      podcast.descripcin = podcData3.descripcin;
                      podcast.titulo = podcData3.titulo;
                      podcast.ponente = podcData3.ponente;
                      podcast.linkAudio = podcData3.linkAudio;
                      podcast.id = podcData3.id;
                      podcast.posicion = cambio2;

                      _player.stop();
                      playBtn = Icons.play_arrow;
                      playing = false;
                      seekToSec(0);
                    }
                  });
                },
                icon: Icon(
                  Icons.skip_next,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _crearTexto(PodCastModel podcast) {
    final _screenSize = MediaQuery.of(context).size;

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        child: Text(
          '${podcast.descripcin}',
          style: TextStyle(fontSize: 17, color: Colors.white),
          textAlign: TextAlign.justify,
          overflow: TextOverflow.ellipsis,
          maxLines: 4,
        ),
      ),
    );
  }
}
