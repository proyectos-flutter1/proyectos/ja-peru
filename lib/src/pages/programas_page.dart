import 'package:flutter/material.dart';
//import 'package:ja_peru/src/bloc/provider.dart';

import 'package:ja_peru/src/models/programa_model.dart';

import 'package:ja_peru/src/providers/programas_provider.dart';
import 'package:ja_peru/src/theme/theme.dart';
import 'package:ja_peru/src/widgets/menu_widget.dart';
import 'package:provider/provider.dart';


class ProgramasPage extends StatefulWidget { // lo convertir en Statefull, porque noa ctualizaba las imagenes que subia
   
  @override
  _ProgramasPageState createState() => _ProgramasPageState();
}

class _ProgramasPageState extends State<ProgramasPage> {
  final programasProvider = new ProgramasProvider();
   var color1;

  @override
  Widget build(BuildContext context) {

    //final bloc = Provider.of(context);
        final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;
              if(appTheme2.scaffoldBackgroundColor == Color(0xff16202B)){
              color1 = Colors.white24;
             
              }
              else{
              color1 = Colors.black26;
              
              }

    return Scaffold(
      appBar: AppBar(
        title: Text('Programas')
      ),
      drawer: MenuWidget(),
      body: _crearListado(),

    );
  }

  Widget _crearListado() {
    final _screeSize = MediaQuery.of(context).size; 
    return FutureBuilder(
      future: programasProvider.cargarProgramas(),
      builder: (BuildContext context, AsyncSnapshot<List<ProgramasModel>> snapshot) {
        if ( snapshot.hasData ) {

          final programas = snapshot.data;
          
          return SizedBox(
            
                //height: _screeSize.height-105,
                child: ListView.builder(
               
                
                itemCount: programas.length,
                itemBuilder: (context, i) => _crearItem(context, programas[i] ),
              ),
          );


        } else {
          return Center( child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, ProgramasModel programa ) {

    return  Container(
        child: Column(
          children: <Widget>[
            
            ( programa.logo == null ) 
              ? Image(image: AssetImage('assets/no-image.png'))
              : 
              
              _cardTipo2(programa),

              


          ],
        ),
      );



    

  }

  
  
  Widget _cardTipo2( ProgramasModel programa ) {

    final _screenSize = MediaQuery.of(context).size; 
   
     final card = Container(
            child: Column(
              children: <Widget>[
                
                  FadeInImage(
                    image: NetworkImage(programa.poster ),
                    placeholder: AssetImage('assets/jar-loading.gif'),
                    fadeInDuration: Duration(milliseconds:  200),
                    height: 250,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),

            
            
                       ListTile(
                         
                title: Text('${ programa.idprograma }',overflow: TextOverflow.ellipsis, style: TextStyle(color:Colors.black)),
                subtitle: Container(
                  child: Row(
                    children: <Widget>[
                      if(programa.estado=='Activo') Icon(Icons.circle,color: Colors.green, size:10.0,),
                      if(programa.estado=='En Proceso') Icon(Icons.circle,color: Colors.blue, size:10.0,),
                      if(programa.estado=='Finalizado') Icon(Icons.circle,color: Colors.red, size:10.0,),
                      SizedBox(width:2.0),
                      Text( programa.estado,style: TextStyle(color:Colors.black54) ),
                    ], 
                    )
                  ),
                //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
                onTap: () => Navigator.pushNamed(context, 'programa',     arguments: programa    ).then((value) {   
                  setState(() {
                });  }),
              ),
            
            

              ],
                  
            ),
     );
         return Column(
           children:<Widget> [
SizedBox(height:18.0),
Container(
             width: _screenSize.width-25,
            // padding: EdgeInsets.all(10),
             decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(30.0),
               color: Colors.white,
               boxShadow: <BoxShadow>[
                 BoxShadow(
                   color: color1,
                   blurRadius: 10.0,
                   spreadRadius: 2.0,
                   offset: Offset(2.0, 10.0)

                   )          
               ]
                       
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                
                child: card,
              
              
              ),
           ),
SizedBox(height:8),
           ],
                     
         );   

  
}


}
