import 'package:flutter/material.dart';
import 'package:ja_peru/src/theme/theme.dart';
import 'package:ja_peru/src/widgets/menu_widget.dart';
import 'package:ja_peru/src/models/podcast_model.dart';

import 'package:ja_peru/src/providers/podcast_provider.dart';

import 'package:animate_do/animate_do.dart';
import 'package:provider/provider.dart';

class JLabPodcastPage extends StatefulWidget {
  @override
  _JLabPodcastPageState createState() => _JLabPodcastPageState();
}

class _JLabPodcastPageState extends State<JLabPodcastPage> {
  final podcastProvider = new PodcastProvider();

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.primaryColor;
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;

    return Scaffold(
      appBar: AppBar(title: Text('J-Lab Podcast')),
      drawer: MenuWidget(),
      body: _crearListado(appTheme2),
    );
  }

  Widget _crearListado(ThemeData appTheme2) {
    return FutureBuilder(
      future: podcastProvider.cargarPodcast(),
      builder:
          (BuildContext context, AsyncSnapshot<List<PodCastModel>> snapshot) {
        if (snapshot.hasData) {
          final podcasts = snapshot.data;

          return ListView.builder(
            itemCount: podcasts.length,
            itemBuilder: (context, i) => _crearItem(context,
                podcasts[podcasts.length - 1 - i], podcasts, appTheme2),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, PodCastModel podcast, List podcasts,
      ThemeData appTheme2) {
    if (podcast.posicion != 0) {
      return Card(
        child: Column(
          children: <Widget>[
            (podcast.fotoPod == null)
                ? Image(image: AssetImage('assets/no-image.png'))
                : FadeInLeft(
                    child: ListTile(
                      leading: ClipRRect(
                        borderRadius: new BorderRadius.circular(5.0),
                        child: FadeInImage(
                          placeholder: AssetImage('assets/FEDTpyE.gif'),
                          image: NetworkImage(podcast.fotoPod),
                          fit: BoxFit.cover,
                        ),
                      ),

                      title: Text('${podcast.titulo}'),
                      subtitle: Text(podcast.ponente,
                          style: TextStyle(color: Colors.grey)),
                      trailing: Icon(Icons.arrow_forward_ios_sharp,
                          size: 20, color: appTheme2.primaryColor),
                      //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
                      onTap: () => Navigator.pushNamed(context, 'jlabPodcastID',
                              arguments: [podcasts, podcast.posicion])
                          .then((value) {
                        setState(() {});
                      }),
                    ),
                  ),
          ],
        ),
      );
    }
  }
}
