import 'package:flutter/material.dart';
//import 'package:ja_peru/src/bloc/provider.dart';
import 'package:ja_peru/src/models/noticia_model.dart';

import 'package:ja_peru/src/providers/noticias_provider.dart';
import 'package:ja_peru/src/widgets/menu_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget { // lo convertir en Statefull, porque noa ctualizaba las imagenes que subia
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final noticiasProvider = new NoticiaProvider();

  @override
  Widget build(BuildContext context) {

    //final bloc = Provider.of(context);
    

    return Scaffold(
      appBar: AppBar(

        title: Text('Principal')

      ),
      drawer: MenuWidget(),
      body: _crearListado(),
    
    );
  }

  Widget _crearListado() {

    return FutureBuilder(
      future: noticiasProvider.cargarNoticias(),
      builder: (BuildContext context, AsyncSnapshot<List<NoticiaModel>> snapshot) {
        if ( snapshot.hasData ) {

          final noticias = snapshot.data;

          return ListView.builder(
            
            itemCount: noticias.length,
            itemBuilder: (context,i) => _crearItem(context, noticias[noticias.length-1-i] ),
          );

        } else {
          return Center( child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, NoticiaModel noticia ) {

    return Card(
        child: Column(
          children: <Widget>[

            ( noticia.post == null ) 
              ? Image(image: AssetImage('assets/no-image.png'))
              : FadeInImage(
               height: 400,
                
                placeholder: AssetImage('assets/jar-loading.gif'),
                image: NetworkImage( noticia.post ),
                width: double.infinity,
                fit: BoxFit.cover,
              ),
          
            ListTile(
                
              title: Text('${ noticia.titulo }',overflow: TextOverflow.ellipsis,),
              subtitle: Text( noticia.programa,overflow: TextOverflow.ellipsis,style: TextStyle( color: Colors.grey), ),
              //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
              onTap: () =>   _launchURL(noticia),
            ),

          ],
        ),
      );
    


    

  }
  _launchURL(NoticiaModel noticia) async {
  if (await canLaunch(noticia.link)) {
    await launch(noticia.link);
  } else {
    throw 'Could not launch $noticia.link';
  }
}


}