import 'package:flutter/material.dart';
import 'package:ja_peru/src/models/programa_model.dart';
import 'package:ja_peru/src/providers/programas_provider.dart';
import 'package:ja_peru/src/widgets/menu_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class ProgramaIdPage extends StatefulWidget {


  @override
  ProgramaIdPageState createState() => ProgramaIdPageState();
}

class ProgramaIdPageState extends State<ProgramaIdPage> {
 
  final programasProvider = new ProgramasProvider();

  ProgramasModel programa = new ProgramasModel();

  final estiloTitulo    = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size; 
    final ProgramasModel progData = ModalRoute.of(context).settings.arguments;
    if ( progData != null ) {
      programa = progData;
    }
    return Scaffold(
          appBar: AppBar(
        title: Text('${ programa.idprograma }',overflow: TextOverflow.ellipsis,),
        centerTitle: true,
                leading: IconButton(
          iconSize:25,
          icon: const Icon(Icons.arrow_back_rounded),
          onPressed: () {Navigator.of(context).pop(); },
        ) ,
      ),
      //drawer: MenuWidget(),
          body: SingleChildScrollView(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FadeInImage(
                      height: 250,
                      placeholder: AssetImage('assets/giphy.gif'),
                      image: NetworkImage(programa.poster2 ),
                      width: double.infinity,
                      fadeInDuration: Duration(milliseconds:200),
                      fit: BoxFit.cover,

                      ),

                      _crearTitulo(),
                      _crearTexto(),
                     SizedBox(height:5),
                     _crearPM(),
                     _crearBotones(),
                SizedBox( height: 20 ),
                  ],
                ),
            )


      );
  }

Widget _crearTitulo() {

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
        child: Center(
                  child: Row(
            children: <Widget>[

              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('${ programa.idprograma }', style: estiloTitulo ,overflow: TextOverflow.ellipsis,),
                    SizedBox( height: 7.0 ),
                    Row(
                    children: <Widget>[
                      if(programa.estado=='Activo') Icon(Icons.circle,color: Colors.green, size:10.0,),
                      if(programa.estado=='En Proceso') Icon(Icons.circle,color: Colors.blue, size:10.0,),
                      if(programa.estado=='Finalizado') Icon(Icons.circle,color: Colors.red, size:10.0,),
                      SizedBox(width:2.0),
                      Text( programa.estado,style: TextStyle( color: Colors.grey) ),
                    ], 
                    )
                  ],
                ),
              ),

              CircleAvatar(
                radius: 28,
                backgroundColor: Color(0xff01a745),
                backgroundImage: AssetImage('assets/logomenu1.png') ,
                
                
          ),

            ],
          ),
        ),
      ),
    );
  }
 Widget _crearTexto() {
        final _screenSize = MediaQuery.of(context).size; 
    return SafeArea(
      child: Container(
       
        padding: EdgeInsets.symmetric( horizontal: 30.0 ),
        child: Text(
          
          '${ programa.descripcin }',
          textAlign: TextAlign.justify,
          overflow: TextOverflow.ellipsis,
          maxLines: 10,

          //overflow: TextOverflow.ellipsis,
        ),
      ),
    );

  }

Widget _crearPM() {

    return SafeArea(
      child: Container(
        
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 15.0),
        child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                    Container(
                    width: 100,
                    height: 100,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(60.0),
                                          child: FadeInImage(
                        
                        image: NetworkImage(programa.fotoPm),
                        placeholder: AssetImage('assets/FEDTpyE.gif'),
                        fit:BoxFit.fill, 

                        
                        
                        ),
                    ),
                  ),
                  SizedBox(width:20,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('${ programa.pm }', style: estiloTitulo,overflow: TextOverflow.ellipsis, ),
                    SizedBox( height: 5.0 ),
                    Text( programa.cargo ,overflow: TextOverflow.ellipsis,),
                    SizedBox( height: 5.0 ),
                    Text( programa.correo,style: TextStyle(fontSize: 15.0, color: Colors.grey),overflow: TextOverflow.ellipsis, ),
                  ],
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }

Widget _crearBotones() {

    return SafeArea(
      child: Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RaisedButton(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20) ),
              color: Colors.green,
             child: Row(
               children: <Widget>[
              Icon( Icons.phone,color: Colors.white, ),
                SizedBox( width: 10 ),
                Text('Contáctame', style: TextStyle(fontSize:15, color: Colors.white)),

             ], ),           
            onPressed: (){

              _launchURLWSP();
            }
            
            ),

          SizedBox( width: 10 ),

            RaisedButton(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20) ),
              color: Colors.blue,
             child: Row(
               children: <Widget>[
              Icon( Icons.web,color: Colors.white, ),
                SizedBox( width: 10 ),
                Text('Web JAPerú', style: TextStyle(fontSize:15, color: Colors.white)),

             ], ),           
            onPressed: (){
              _launchURL();
            }
            
            ),


        ],
      ),
    ),
      );
    
  }
    _launchURLWSP() async {
     String cel= '${ programa.celular }';
  String url = 'https://api.whatsapp.com/send?phone=+51%20';
 String urlwsp= '$url''$cel';

  if (await canLaunch(urlwsp)) {
    
    await launch(urlwsp);
  } else {
    throw 'Could not launch $url';
  }
}
  _launchURL() async {
  if (await canLaunch(programa.web)) {
    await launch(programa.web);
  } else {
    throw 'Could not launch $programa.web';
  }
}






}