
import 'dart:convert';


import 'package:http/http.dart' as http;


import 'package:ja_peru/src/models/noticia_model.dart';

class NoticiaProvider {

  final String _url = 'https://japeru-4f886-default-rtdb.firebaseio.com/';




  Future<List<NoticiaModel>> cargarNoticias() async {

    final url  = '$_url/Noticias.json';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<NoticiaModel> noticias = new List();


    if ( decodedData == null ) return [];

    decodedData.forEach( ( id, noti ){

      final noticiasTemp = NoticiaModel.fromJson(noti);
      noticiasTemp.id = id;

      noticias.add( noticiasTemp );

    });

    // print( noticias[0].id );

    return noticias;

  }



  


}

