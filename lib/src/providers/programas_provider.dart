
import 'dart:convert';


import 'package:http/http.dart' as http;


import 'package:ja_peru/src/models/programa_model.dart';

class ProgramasProvider {

  final String _url = 'https://japeru-4f886-default-rtdb.firebaseio.com/';




  Future<List<ProgramasModel>> cargarProgramas() async {

    final url  = '$_url/Programas.json';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<ProgramasModel> programas = new List();


    if ( decodedData == null ) return [];

    decodedData.forEach( ( id, progr ){

      final programasTemp = ProgramasModel.fromJson(progr);
      programasTemp.id = id;

      programas.add( programasTemp );

    });

    // print( programas[0].id );

    return programas;

  }



  


}

