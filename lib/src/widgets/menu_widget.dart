import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:ja_peru/src/theme/theme.dart';
import 'package:provider/provider.dart';

class MenuWidget extends StatelessWidget {
  final colorbase = Color(0xff01a745);
  final _estiloTexto = new TextStyle(fontSize: 15, color: Color(0xff01a745));

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.primaryColor;
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;

    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(),
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage('assets/logomenu1.png'),
                //fit: BoxFit.cover
              )),
            ),

            /*   _CrearListado(),
            SizedBox(height:30.0),
             FadeInImage(
              placeholder: AssetImage('assets/giphy.gif'),
              image: NetworkImage(
                  'https://scontent-lim1-1.xx.fbcdn.net/v/t1.0-9/90972364_3690307037676975_9006296615824130048_o.jpg?_nc_cat=108&ccb=2&_nc_sid=9267fe&_nc_eui2=AeGfGjxeIVkcHbN0lAXad90LfLYxvo65r4l8tjG-jrmviZRfTD602-ugeAkNiIc8xXi5Q0wcWoHxiyAxA6Gh8dqq&_nc_ohc=w5ewBNSt5tkAX_-nEn5&_nc_ht=scontent-lim1-1.xx&oh=f4de68edf4e237f62df11e623ce96775&oe=6038D174'),
              fadeInDuration: Duration(milliseconds: 200),
            ),*/

            /*FadeInLeft(
                delay: Duration(milliseconds:100),
                            child: */
            Container(
              // height: _screenSize.height*0.07,
              child: ListTile(
                leading: Icon(Icons.home_work, color: appTheme2.primaryColor),
                title: Text('Principal', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'home'),
              ),
            ),
            Divider(),
            /* ),
            
         //   
            
            FadeInLeft(
              delay: Duration(milliseconds:200),
                          child: */
            Container(
              // height: _screenSize.height*0.07,
              child: ListTile(
                leading: Icon(Icons.article, color: appTheme2.primaryColor),
                title: Text('Programas', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () =>
                    Navigator.pushReplacementNamed(context, 'programas'),
              ),
            ),
            Divider(),
            /* ),
            //
            FadeInLeft(
              delay: Duration(milliseconds:300),
                          child: */
            Container(
              // height: _screenSize.height*0.07,
              child: ListTile(
                leading: Icon(Icons.group, color: appTheme2.primaryColor),
                title: Text('Equipo', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'equipo'),
              ),
            ),
            Divider(),
            /*),
            FadeInLeft(
              delay: Duration(milliseconds:400),
                          child: */
            Container(
              //height: _screenSize.height*0.08,
              child: ListTile(
                leading: Icon(Icons.headset_mic_rounded,
                    color: appTheme2.primaryColor),
                title:
                    Text('J-LAB Podcast', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () =>
                    Navigator.pushReplacementNamed(context, 'jlabPodcast'),
              ),
            ),
            Divider(),
            /*),
                      FadeInLeft(
              delay: Duration(milliseconds:500),
                          child: */
            Container(
              //height: _screenSize.height*0.08,
              child: ListTile(
                leading: Icon(Icons.video_collection_outlined,
                    color: appTheme2.primaryColor),
                title: Text('Videos JA', style: TextStyle(color: accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: appTheme2.primaryColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'video'),
              ),
            ),
            Divider(),
            /* ),
            FadeInLeft(
              delay: Duration(milliseconds:500),
                          child: (*/
            Container(
              //  height: _screenSize.height*0.07,
              child: ListTile(
                  leading: Icon(Icons.double_arrow_outlined,
                      color: appTheme2.primaryColor),
                  title: Text('Salir', style: TextStyle(color: accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,
                      color: appTheme2.primaryColor),
                  onTap: () {
                    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  }),
            ),
            // ),
            Divider(),
            SizedBox(height: 20),

            ListTile(
              leading: Icon(Icons.lightbulb_outline, color: accentColor),
              title: Text('Dark Mode', style: TextStyle(color: accentColor)),
              trailing: Switch.adaptive(
                  value: appTheme.darkTheme,
                  activeColor: accentColor,
                  onChanged: (value) => appTheme.darkTheme = value),
            ),

            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen, color: accentColor),
                title:
                    Text('Custom Theme', style: TextStyle(color: accentColor)),
                trailing: Switch.adaptive(
                    value: appTheme.customTheme,
                    activeColor: accentColor,
                    onChanged: (value) => appTheme.customTheme = value),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
/*
class _CrearListado extends StatelessWidget {
  
      final menuProvider = new MenuProvider();
    final colorbase    = Color(0xff01a745);
  final _estiloTexto =new TextStyle(fontSize:15, color: Color(0xff01a745));
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: menuProvider.cargarMenu() ,
      builder: (BuildContext context, AsyncSnapshot<List<MenuModel>> snapshot) {
        if ( snapshot.hasData ) {

          final menu = snapshot.data;

          return ListView.builder(
            shrinkWrap: true,//Soluciono mi    
            itemCount: menu.length,
            itemBuilder: (context, i) => _crearItem(context, menu[i] , i),
          );

        } else {
          return Column(
            children: [
               //Center( child: CircularProgressIndicator(),),
               //Container(height:244)
              Container(height:280)         
              ],
              );
            
            
        }
      },
    );
  }
     Widget _crearItem(BuildContext context, MenuModel menu, int i ) {

      
      return  FadeInLeft(
        delay: Duration(milliseconds:100*i),
              child: ListTile(
                  leading: getIcon('${ menu.icon }'),
                  title: Text('${ menu.texto }'),
                  trailing: Icon(Icons.keyboard_arrow_right,color: colorbase),
                  //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
                  onTap: () => Navigator.pushNamed(context, menu.ruta),
                ),
      );
      

  } 
  
}
*/