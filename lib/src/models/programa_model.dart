// To parse this JSON data, do
//
//     final programasModel = programasModelFromJson(jsonString);

import 'dart:convert';

ProgramasModel programasModelFromJson(String str) => ProgramasModel.fromJson(json.decode(str));

String programasModelToJson(ProgramasModel data) => json.encode(data.toJson());

class ProgramasModel {
    ProgramasModel({
        this.id,
        this.cargo,
        this.correo,
        this.descripcin,
        this.idprograma,
        this.logo,
        this.pm,
        this.poster,
        this.poster2,
        this.video,
        this.celular,
        this.estado,
        this.fotoPm,
        this.web,

    });

    String id;
    String cargo;
    String correo;
    String descripcin;
    String idprograma;
    String logo;
    String pm;
    String poster;
    String poster2;
    String video;
    int celular;
    String estado;
    String fotoPm;
    String web;

    factory ProgramasModel.fromJson(Map<String, dynamic> json) => ProgramasModel(
        id          : json["id"],
        cargo       : json["Cargo"],
        correo      : json["Correo"],
        descripcin  : json["Descripción"],
        idprograma  : json["IdPrograma"],
        logo        : json["Logo"],
        pm          : json["PM"],
        poster      : json["Poster"],
        poster2     : json["Poster2"],
        video       : json["Video"],
        celular     : json["celular"],
        estado      : json["estado"],
        fotoPm      : json["fotoPM"],
        web         : json["web"],
    );

    Map<String, dynamic> toJson() => {
        "id"          : id,
        "Cargo"       : cargo,
        "Correo"      : correo,
        "Descripción" : descripcin,
        "IdPrograma"  : idprograma,
        "Logo"        : logo,
        "PM"          : pm,
        "Poster"      : poster,
        "Poster2"     : poster2,
        "Video"       : video,
        "celular"     : celular,
        "estado"      : estado,
        "fotoPM"      : fotoPm,
        "web"         : web,
    };
}
