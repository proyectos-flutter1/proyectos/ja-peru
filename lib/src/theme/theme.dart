import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier {
  bool _darkTheme = false;
  bool _customTheme = false;

  ThemeData _currentTheme;

  bool get darkTheme => this._darkTheme;
  bool get customTheme => this._customTheme;
  ThemeData get currentTheme => this._currentTheme;

  ThemeChanger(int theme) {
    switch (theme) {
      case 1: // light
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light().copyWith(
          // accentColor: Color(0xff01a745),
          appBarTheme: AppBarTheme(color: Color(0xff01a745)),
          primaryColor: Color(0xff01a745),
          textTheme: TextTheme(
            bodyText1: TextStyle(color: Color(0xff01a745)),
            subtitle1: TextStyle(color: Colors.black),
            bodyText2: TextStyle(color: Colors.black),
          ),
        );
        break;

      case 2: // Dark
        _darkTheme = true;
        _customTheme = false;
        /* _currentTheme = ThemeData.dark().copyWith(
          accentColor: Color(0xff01a745)
        );*/
        break;

      case 3: // Dark
        _darkTheme = false;
        _customTheme = true;
        break;

      default:
        _darkTheme = false;
        _darkTheme = false;
        _currentTheme = ThemeData.light();
    }
  }

  set darkTheme(bool value) {
    _customTheme = false;
    _darkTheme = value;

    if (value) {
      _currentTheme = ThemeData.dark().copyWith(
        // accentColor: Colors.white,
        primaryColorLight: Colors.white,
        primaryColor: Colors.white,
        scaffoldBackgroundColor: Color(0xff16202B),
        textTheme: TextTheme(bodyText1: TextStyle(color: Colors.white)),
      );
    } else {
      _currentTheme = ThemeData.light().copyWith(
        // accentColor: Color(0xff01a745),
        appBarTheme: AppBarTheme(color: Color(0xff01a745)),
        primaryColor: Color(0xff01a745),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Color(0xff01a745)),
          subtitle1: TextStyle(color: Colors.black),
          bodyText2: TextStyle(color: Colors.black),
        ),
      );
    }

    notifyListeners();
  }

  set customTheme(bool value) {
    _customTheme = value;
    _darkTheme = false;

    if (value) {
      _currentTheme = ThemeData.light().copyWith(
        // accentColor: Colors.blueAccent,
        primaryColor: Colors.blueAccent,
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Colors.blueAccent),
          subtitle1: TextStyle(color: Colors.black),
          bodyText2: TextStyle(color: Colors.black),
        ),
      );
    } else {
      _currentTheme = ThemeData.light().copyWith(
        // accentColor: Color(0xff01a745),
        appBarTheme: AppBarTheme(color: Color(0xff01a745)),
        primaryColor: Color(0xff01a745),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Color(0xff01a745)),
          subtitle1: TextStyle(color: Colors.black),
          bodyText2: TextStyle(color: Colors.black),
        ),
      );
    }

    notifyListeners();
  }
}
