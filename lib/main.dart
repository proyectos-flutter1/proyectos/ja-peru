import 'package:flutter/material.dart';
import 'package:ja_peru/src/pages/equipo_page.dart';

//import 'package:formvalidation/src/bloc/provider.dart';

import 'package:ja_peru/src/pages/home_page.dart';
import 'package:ja_peru/src/pages/jlab_id_podcast_page.dart';
import 'package:ja_peru/src/pages/splash_screen.dart';
import 'package:ja_peru/src/pages/videos_page.dart';
import 'package:ja_peru/src/pages/jlab_podcast_page.dart';
import 'package:ja_peru/src/pages/programa_id_page.dart';
import 'package:ja_peru/src/pages/programas_page.dart';

import 'package:ja_peru/src/theme/theme.dart';
import 'package:provider/provider.dart';

void main() => runApp(
    ChangeNotifierProvider(create: (_) => new ThemeChanger(1), child: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    //return Provider(
    //child: MaterialApp(
    return MaterialApp(
      theme: currentTheme,
      debugShowCheckedModeBanner: false,
      title: 'JA PERÚ',
      initialRoute: 'splash',
      routes: {
        'splash': (BuildContext context) => SplashScreenPage(),
        'home': (BuildContext context) => HomePage(),
        'programas': (BuildContext context) => ProgramasPage(),
        'programa': (BuildContext context) => ProgramaIdPage(),
        'equipo': (BuildContext context) => EquipoPage(),
        'video': (BuildContext context) => VideosPage(),
        'jlabPodcast': (BuildContext context) => JLabPodcastPage(),
        'jlabPodcastID': (BuildContext context) => PodcastIdPage(),
      },
      /*theme: ThemeData(
          primaryColor: Color(0xff01a745),
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Color(0xff01a745)
        )
        ),*/
    );
    //);
  }
}
